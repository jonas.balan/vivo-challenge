# Vivo Challenge

This is my solution to this challenge. 

## Install environment
  
### Postgres
  I used Postgres database in my local docker instance, to now more about this, click [here](https://www.google.com).<br/>
  I used too, PgAdmin4, that's a IDE to postgres, and in run in my local docker too.<br/>
  After install the both tools ,in pgadmin connect in server database and create a new database instance.

### Config Connection String
  The file to config this is in "src\Vivo.Challenge.API\appsettings.Development.json", you have to edit this file and config a connection string.

### Install Net 5.0 SDK
   To compile you need to install Net SDK, this is the [link](https://dotnet.microsoft.com/download/dotnet/5.0).<br/>

### Run Migrantions to update database
  install dotnet-ef ```dotnet tool install --global dotnet-ef```<br/>
  You have run this command from "src\Vivo.Challenge.Repository"<br/>
  ```dotnet ef database update --startup-project ..\Vivo.Challenge.API```

### Run 
 
  In the repository directory, run this:<br/>
  ``` cd Vivo.Challenge.API ```<br/>
  ```dotnet run```<br/>
  The server wil be exposed in [http://localhost:5000/swagger](http://localhost:5000/swagger)
## Project Steps

### 1 - Choising Stack of Tools
  This project was make in NET 5, ASPNet Core, EF Core, EF Migrations and Postgres database.

### 2 - Identify entities and map corellationsships 

### 3 - Design and create unit tests

### 4 - implement model and service layer

### 5 - implement repository layer

### 6 - implement API layer

