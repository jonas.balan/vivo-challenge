﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Vivo.Challenge.Service;

namespace Vivo.Challenge.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BotsController : ControllerBase
    {
        private readonly IBotService _botService;
        public BotsController(IBotService botService)
        {
            _botService = botService;
        }

        [HttpGet]
        public async Task<IEnumerable<Bot>> Get()
        {
            return await _botService.Get();
        }

        [HttpGet("{id}")]
        public async Task<Bot> Get(Guid id)
        {
            return await _botService.Get(id);
        }

        [HttpPost]
        public async Task<Bot> Create([FromBody] Bot bot)
        {
            return await _botService.Create(bot);
        }

        [HttpPut]
        public async Task Update([FromBody] Bot bot)
        {
            await _botService.Update(bot);
        }

        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            await _botService.Delete(id);
        }
    }
}
