﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Vivo.Challenge.Service;

namespace Vivo.Challenge.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly IMessageService _messageService;
        public MessagesController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        //[HttpGet()]
        //public async Task<IEnumerable<Message>> Get()
        //{
        //    return await _messageService.Get();
        //}

        [HttpGet("{id}")]
        public async Task<Message> Get(Guid id)
        {
            return await _messageService.Get(id);
        }

        [HttpGet()]
        public async Task<IEnumerable<Message>> GetByConversationId([FromQuery(Name = "conversationId")] Guid conversationId)
        {
            return await _messageService.GetByConversationId(conversationId);
        }


        [HttpPost]
        public async Task<Message> Create([FromBody] Message message)
        {            
            return await _messageService.Create(message);
        }

    }
}
