﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vivo.Challenge.Common
{
    public interface IDefaulCrudOperations<T>
    {
        Task<IEnumerable<T>> Get();

        Task<T> Get(Guid id);

        Task<T> Create(T entity);

        Task Update(T entity);

        Task Delete(Guid id);
    }
}
