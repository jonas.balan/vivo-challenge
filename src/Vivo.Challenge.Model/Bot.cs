﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Vivo.Challenge.Model
{
    public class Bot : BaseEntity
    {       
        public string Name { get; set; }

        [JsonIgnore]
        public ICollection<MessageFromBot> MessagesFrom { get; set; }

        [JsonIgnore]
        public ICollection<MessageToBot> MessagesTo { get; set; }

    }
}
