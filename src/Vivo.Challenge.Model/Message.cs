﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Vivo.Challenge.Model
{
    public enum BotMessageDirectionEnum
    { 
      From, To
    }


    public class Message : BaseEntity
    {           
        public Guid ConversationId { get; set; }
        public DateTime Timestamp { get; set; }
        public Guid From { get; set; }
        public Guid To { get; set; }
        public string Text { get; set; }

        [JsonIgnore]
        public BotMessageDirectionEnum BotMessageDirection { get; set; }

    }
}
