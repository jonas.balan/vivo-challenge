﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Vivo.Challenge.Model
{
    public class MessageToBot: Message
    {
        [JsonIgnore]
        public virtual Bot BotTo { get; set; }

    }
}
