﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vivo.Challenge.Repository
{
    public class BaseRepository<T> : IBaseRepository<T>  where T : class
    {
        protected readonly ChallengeContext _challengeContext;
        public BaseRepository(ChallengeContext challengeContext)
        {
            _challengeContext = challengeContext;
        }

        public virtual async Task<IEnumerable<T>> Get()
        {
            return await Task.FromResult(_challengeContext.Set<T>());
        }

        public virtual async Task<T> Get(Guid id)
        {
            return await _challengeContext.FindAsync<T>(id);
        }

        public virtual async Task<T> Create(T entity)
        {
            var result = await _challengeContext.AddAsync(entity);
            await _challengeContext.SaveChangesAsync();
            return (T)result.Entity;
        }
        public virtual async Task Update(T entity)
        {
            _challengeContext.Update(entity);
            await _challengeContext.SaveChangesAsync();
        }

        public virtual async Task Delete(Guid id)
        {
            var entity = await Get(id);
            _challengeContext.Remove(entity);
            await _challengeContext.SaveChangesAsync();
        }

        

      
    }
}
