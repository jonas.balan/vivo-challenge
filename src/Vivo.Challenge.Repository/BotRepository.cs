﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Repository
{
    public class BotRepository : BaseRepository<Bot>, IBotRepository
    {
        public BotRepository(ChallengeContext challengeContext) : base(challengeContext)
        {
        }
    }
}
