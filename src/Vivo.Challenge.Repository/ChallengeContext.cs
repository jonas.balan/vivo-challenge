﻿using Microsoft.EntityFrameworkCore;
using Vivo.Challenge.Model;
using Vivo.Challenge.Repository.EntityConfiguration;

namespace Vivo.Challenge.Repository
{
    public class ChallengeContext : DbContext
    {
        public DbSet<Bot> Bots { get; set; }
        public DbSet<MessageToBot> MessagesToBot { get; set; }

        public DbSet<MessageFromBot> MessagesFromBot { get; set; }
        public ChallengeContext(DbContextOptions<ChallengeContext> dbContextOptions) : base(dbContextOptions)
        {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BotConfiguration());
            modelBuilder.ApplyConfiguration(new MessageToBotConfiguration());
            modelBuilder.ApplyConfiguration(new MessageFromBotConfiguration());
        }
    }
}
