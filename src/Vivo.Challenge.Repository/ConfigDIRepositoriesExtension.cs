﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Repository
{
    public static class ConfigDIRepositoriesExtension
    {
        public static void ConfigDIRepositories(this IServiceCollection services)
        {
            services.AddScoped<IBotRepository, BotRepository>();
            services.AddScoped<IMessageRepository, MessageRepository>();
            services.AddScoped<IMessageFromBotRepository, MessageFromBotRepository>();
            services.AddScoped<IMessageToBotRepository, MessageToBotRepository>();
            
            services
                //.AddEntityFrameworkNpgsql()
                .AddDbContext<ChallengeContext>((serviceProvider, options) =>
                options.UseNpgsql(serviceProvider.GetService<IConfiguration>().GetConnectionString("ChallengeDb")));

           
        }
    }
}
