﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Repository.EntityConfiguration
{
    public class BotConfiguration : IEntityTypeConfiguration<Bot>
    {
        public void Configure(EntityTypeBuilder<Bot> builder)
        {
            builder.HasKey(bot => bot.Id);
            builder.HasMany(x => x.MessagesFrom).WithOne(x => x.BotFrom).HasForeignKey(x => x.From).IsRequired(false).OnDelete(DeleteBehavior.NoAction); ;
            builder.HasMany(x => x.MessagesTo).WithOne(x => x.BotTo).HasForeignKey(x => x.To).IsRequired(false).OnDelete(DeleteBehavior.NoAction); ;
        }
    }
}
