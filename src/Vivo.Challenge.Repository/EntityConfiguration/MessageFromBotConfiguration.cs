﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Repository.EntityConfiguration
{
    public class MessageFromBotConfiguration : IEntityTypeConfiguration<MessageFromBot>
    {
        public void Configure(EntityTypeBuilder<MessageFromBot> builder)
        {
            builder.HasKey(message =>  message.Id );
            builder.HasOne(x => x.BotFrom)
                   .WithMany(x => x.MessagesFrom)
                   .HasForeignKey(x => x.From)
                   .IsRequired(true);
            builder.Property(x => x.From).IsRequired(true);
            builder.Ignore(x => x.BotMessageDirection);
            builder.HasIndex(x => x.ConversationId);
        }
    }
}
