﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Repository.EntityConfiguration
{
    public class MessageToBotConfiguration : IEntityTypeConfiguration<MessageToBot>
    {
        public void Configure(EntityTypeBuilder<MessageToBot> builder)
        {
            builder.HasKey(message =>  message.Id );
            builder.HasOne(x => x.BotTo)
                   .WithMany(x => x.MessagesTo)
                   .HasForeignKey(x => x.To)
                   .IsRequired(true);
            builder.Property(x => x.To).IsRequired(true);
            builder.Ignore(x => x.BotMessageDirection);
            builder.HasIndex(x => x.ConversationId);
        }
    }
}
