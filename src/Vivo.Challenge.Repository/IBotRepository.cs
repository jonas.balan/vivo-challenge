﻿using System;
using Vivo.Challenge.Common;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Repository
{
    public interface IBotRepository : IBaseRepository<Bot>
    {

    }
}
