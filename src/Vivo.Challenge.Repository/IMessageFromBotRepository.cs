﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Repository
{
    public interface IMessageFromBotRepository : IBaseRepository<MessageFromBot>
    {
        Task<IEnumerable<MessageFromBot>>  GetByConversationId(Guid conversationId);
    }
}
