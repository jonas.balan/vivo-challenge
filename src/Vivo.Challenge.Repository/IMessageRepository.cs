﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Repository
{
    public interface IMessageRepository : IBaseRepository<Message>
    {
        Task<IEnumerable<Message>> GetByConversationId(Guid conversationId);
    }
}
