﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Vivo.Challenge.Repository
{
    public class MessageFromBotRepository : BaseRepository<MessageFromBot>, IMessageFromBotRepository
    {
        public MessageFromBotRepository(ChallengeContext challengeContext) : base(challengeContext)
        { }

        public async Task<IEnumerable<MessageFromBot>> GetByConversationId(Guid conversationId)
        {
            return await Task.FromResult(this._challengeContext.MessagesFromBot.Where(x => x.ConversationId == conversationId));
        }
    }
}
