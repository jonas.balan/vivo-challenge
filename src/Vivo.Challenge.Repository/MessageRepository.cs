﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using System.Linq;

namespace Vivo.Challenge.Repository
{
    public class MessageRepository : BaseRepository<Message>, IMessageRepository
    {
        private readonly IServiceProvider _serviceProvider;
        public MessageRepository(ChallengeContext challengeContext, IServiceProvider serviceProvider) : base(challengeContext)
        {
            _serviceProvider = serviceProvider;
        }
        public override async Task<IEnumerable<Message>> Get()
        {
            var repToBot = _serviceProvider.GetService<IMessageToBotRepository>();
            var taskGetToBot = repToBot.Get();
            var repFromBot = _serviceProvider.GetService<IMessageFromBotRepository>();
            var taskGetFromBot = repFromBot.Get();
            await Task.WhenAll(taskGetToBot, taskGetFromBot);
            var result = taskGetToBot.Result.Concat<Message>(taskGetFromBot.Result);
            return result.OrderBy(x=>x.Timestamp);
        }

        public override async Task<Message> Get(Guid id)
        {           
            var repToBot = _serviceProvider.GetService<IMessageToBotRepository>();
            var result = await repToBot.Get(id);
            if (result == null)
            {
                var repFromBot = _serviceProvider.GetService<IMessageFromBotRepository>();
                return await repFromBot.Get(id);
            }
            return result;
        }

        public override async Task<Message> Create(Message entity)
        {
            var repositoryPartial = RepositoryFactory(entity);
            return await repositoryPartial.Create(GetEntityTypeByModel(entity));
        }

        public async Task<IEnumerable<Message>> GetByConversationId(Guid conversationId)
        {
            var repToBot = _serviceProvider.GetService<IMessageToBotRepository>();
            var taskGetToBot = repToBot.GetByConversationId(conversationId);
            var repFromBot = _serviceProvider.GetService<IMessageFromBotRepository>();
            var taskGetFromBot = repFromBot.GetByConversationId(conversationId);
            await Task.WhenAll(taskGetToBot, taskGetFromBot);
            var result = taskGetToBot.Result.Concat<Message>(taskGetFromBot.Result);
            return result.OrderBy(x => x.Timestamp);
        }

        public override Task Update(Message entity)
        {
            throw new NotImplementedException();
        }

        public override Task Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        private dynamic RepositoryFactory(Message entity)
        {           
            if (entity.BotMessageDirection == BotMessageDirectionEnum.From)
                return _serviceProvider.GetService<IMessageFromBotRepository>() ;
            else
                return  _serviceProvider.GetService<IMessageToBotRepository>();
        }

        private dynamic GetEntityTypeByModel(Message message)
        {            
            if (message.BotMessageDirection == BotMessageDirectionEnum.From)
                return Map<MessageFromBot>(message);
            else
                return Map<MessageToBot>(message);
        }

        private T Map<T>(Message message)
        {
            var config = new MapperConfiguration(x => x.CreateMap<Message, T>());
            var mapper = new Mapper(config);
            return mapper.Map<T>(message);
        }

     
    }
}
