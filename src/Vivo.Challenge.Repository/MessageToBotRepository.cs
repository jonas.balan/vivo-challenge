﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Vivo.Challenge.Repository
{
    public class MessageToBotRepository : BaseRepository<MessageToBot>, IMessageToBotRepository
    {
        public MessageToBotRepository(ChallengeContext challengeContext) : base(challengeContext)
        { }


        public async Task<IEnumerable<MessageToBot>> GetByConversationId(Guid conversationId)
        {
            return await Task.FromResult(this._challengeContext.MessagesToBot.Where(x => x.ConversationId == conversationId));
        }
    }
}
