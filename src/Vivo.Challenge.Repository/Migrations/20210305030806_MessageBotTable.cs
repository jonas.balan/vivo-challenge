﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Vivo.Challenge.Repository.Migrations
{
    public partial class MessageBotTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MessagesFromBot",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ConversationId = table.Column<Guid>(type: "uuid", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    From = table.Column<Guid>(type: "uuid", nullable: false),
                    To = table.Column<Guid>(type: "uuid", nullable: false),
                    Text = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessagesFromBot", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MessagesFromBot_Bots_From",
                        column: x => x.From,
                        principalTable: "Bots",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MessagesToBot",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ConversationId = table.Column<Guid>(type: "uuid", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    From = table.Column<Guid>(type: "uuid", nullable: false),
                    To = table.Column<Guid>(type: "uuid", nullable: false),
                    Text = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessagesToBot", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MessagesToBot_Bots_To",
                        column: x => x.To,
                        principalTable: "Bots",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_MessagesFromBot_ConversationId",
                table: "MessagesFromBot",
                column: "ConversationId");

            migrationBuilder.CreateIndex(
                name: "IX_MessagesFromBot_From",
                table: "MessagesFromBot",
                column: "From");

            migrationBuilder.CreateIndex(
                name: "IX_MessagesToBot_ConversationId",
                table: "MessagesToBot",
                column: "ConversationId");

            migrationBuilder.CreateIndex(
                name: "IX_MessagesToBot_To",
                table: "MessagesToBot",
                column: "To");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MessagesFromBot");

            migrationBuilder.DropTable(
                name: "MessagesToBot");
        }
    }
}
