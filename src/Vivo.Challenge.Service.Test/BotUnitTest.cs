using System.Threading.Tasks;
using Vivo.Challenge.Model;
using FluentAssertions;
using Xunit;
using System.Collections.Generic;
using Vivo.Challenge.Service.Test.Mock.Data;

namespace Vivo.Challenge.Service.Test
{
    public class BotUnitTest : DefaultCrudUnitTest<Bot, IBotService>, IClassFixture<DIFixture>
    {       
        public BotUnitTest(DIFixture fixture) :  base(fixture)
        {}

        [Theory]
        [ClassData(typeof(BotTestData))]
        public override async Task<Bot> CreationEntitiesTest(Bot entity)
        {
            var resultMessage = await base.CreationEntitiesTest(entity);
            resultMessage.Name.Should().Be(entity.Name);
            return resultMessage;
        }

        [Fact]
        public override async Task<IEnumerable<Bot>> GetAllEntitiesTest()
        {
            return await base.GetAllEntitiesTest();
        }

        [Theory]
        [ClassData(typeof(BotTestData))]
        public override async Task<Bot> GetEntitiesTest(Bot entity)
        {
            var resultMessage = await base.GetEntitiesTest(entity);
            resultMessage.Name.Should().Be(entity.Name);
            return resultMessage;
        }

        [Theory]
        [ClassData(typeof(BotTestData))]
        public override async Task<Bot> UpdateEntitiesTest(Bot entity)
        {
            entity.Name += "_teste";
            var messageUpdated = await base.UpdateEntitiesTest(entity);
            messageUpdated.Name.Should().Be(entity.Name);
            return messageUpdated;
        }

        [Theory]
        [ClassData(typeof(BotTestData))]
        public override async Task DeleteBotTest(Bot entity)
        {
            await base.DeleteBotTest(entity);
        }       

    }
}
