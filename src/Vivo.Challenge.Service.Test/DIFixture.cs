﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Vivo.Challenge.Repository;
using Vivo.Challenge.Service.Test.Mock.Repository;

namespace Vivo.Challenge.Service.Test
{
    public class DIFixture : IDisposable
    {
        private readonly IHost _host;
       

        public DIFixture()
        {
            var builder = Host.CreateDefaultBuilder(null)
                .ConfigureServices(services =>
                {
                    services.ConfigDIServices();
                    services.ConfigDIRepositoriesMock();
                });

            _host = builder.Build();
            _host.Start();           
        }

        internal IServiceProvider CreateScope()
        {
            return _host.Services.CreateScope().ServiceProvider;
        }

        public void Dispose()
        {
            _host.Dispose();         
        }
    }
}
