﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Service.Test
{
    public class DefaultCrudUnitTest<T, ServiceType> where T : BaseEntity where ServiceType : IBaseService<T>
    {
        private readonly DIFixture _fixture;

        public DefaultCrudUnitTest(DIFixture fixture)
        {
            _fixture = fixture;
        }

        private ServiceType GetEntityService(IServiceProvider services)
        {
            return services.GetService<ServiceType>();
        }

        public virtual async Task<IEnumerable<T>> GetAllEntitiesTest()
        {
            var services = _fixture.CreateScope();
            var serviceEntity = GetEntityService(services);
            var entities = await serviceEntity.Get();
            entities.Should().NotBeNull();
            var entityDataMock = services.GetService<List<T>>();
            entities.Should().Contain(entityDataMock);
            return entities;
        }
      
        public virtual async Task<T> GetEntitiesTest(T entity)
        {
            var services = _fixture.CreateScope();
            var serviceEntity = GetEntityService(services);
            var entityFromDb = await serviceEntity.Get(entity.Id);
            entityFromDb.Should().NotBeNull();
            entityFromDb.Id.Should().NotBeEmpty();
            entityFromDb.Id.Should().Be(entity.Id);           
            return entityFromDb;
        }
      
        public virtual async Task<T> CreationEntitiesTest(T entity)
        {
            var services = _fixture.CreateScope();
            var serviceEntity = GetEntityService(services);
            var createdEntity = await serviceEntity.Create(entity);
            createdEntity.Should().NotBeNull();
            createdEntity.Id.Should().NotBeEmpty();         
            return createdEntity;
        }

      
        public virtual async Task<T> UpdateEntitiesTest(T entity)
        {
            var services = _fixture.CreateScope();
            var serviceEntity = GetEntityService(services);           
            await serviceEntity.Update(entity);
            var updatedBot = await serviceEntity.Get(entity.Id);
            updatedBot.Should().NotBeNull();
            updatedBot.Id.Should().NotBeEmpty();
            updatedBot.Id.Should().Be(entity.Id);           
            return updatedBot;
        }

        public virtual async Task DeleteBotTest(T entity)
        {
            var services = _fixture.CreateScope();
            var serviceEntity = GetEntityService(services);
            await serviceEntity.Delete(entity.Id);
            var updatedBot = await serviceEntity.Get(entity.Id);
            updatedBot.Should().BeNull();
        }
    }
}
