﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Vivo.Challenge.Service.Test.Mock.Data;
using Xunit;

namespace Vivo.Challenge.Service.Test
{
    public class MessageUnitTest : DefaultCrudUnitTest<Message, IMessageService>, IClassFixture<DIFixture>
    {
        public MessageUnitTest(DIFixture fixture) : base(fixture)
        { }

        [Theory]
        [ClassData(typeof(MessageTestData))]
        public override async Task<Message> CreationEntitiesTest(Message entity)
        {
            var resultMessage = await base.CreationEntitiesTest(entity);
            resultMessage.Text.Should().Be(entity.Text);
            return resultMessage;
        }

        [Fact]
        public override async Task<IEnumerable<Message>> GetAllEntitiesTest()
        {
            return await base.GetAllEntitiesTest();
        }

        [Theory]
        [ClassData(typeof(MessageTestData))]
        public override async Task<Message> GetEntitiesTest(Message entity)
        {
            var resultMessage =  await base.GetEntitiesTest(entity);
            resultMessage.Text.Should().Be(entity.Text);
            return resultMessage;
        }

        [Theory]
        [ClassData(typeof(MessageTestData))]
        public override async Task<Message> UpdateEntitiesTest(Message entity)
        {
            entity.Text += "_teste";
            var messageUpdated =  await base.UpdateEntitiesTest(entity);
            messageUpdated.Text.Should().Be(entity.Text);
            return messageUpdated;
        }

        [Theory]
        [ClassData(typeof(MessageTestData))]
        public override async Task DeleteBotTest(Message entity)
        {
            await base.DeleteBotTest(entity);
        }
    }
}
