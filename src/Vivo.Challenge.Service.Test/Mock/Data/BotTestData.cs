﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Service.Test.Mock.Data
{
    internal class BotTestData : IEnumerable<object[]>
    {
       
        public IEnumerator<object[]> GetEnumerator()
        {            
            return GetBots().Select(bot => new object[] { bot }).GetEnumerator();
        }


        public IEnumerable<Bot> GetBots()
        {
            yield return new Bot() { Name = "Aureo"       ,Id = Guid.Parse("{07307036-ED50-4031-8335-2C1ECFCC8DD7}") } ;
            yield return  new Bot() { Name = "Nitrogen"   ,Id = Guid.Parse("{67717226-0A6A-412C-8C12-54A32D7FB4DE}") };
            yield return  new Bot() { Name = "Olanzapine" ,Id = Guid.Parse("{384B56F1-D17F-4250-BB0B-14FF9C21C8DB}") };
            yield return  new Bot() { Name = "Synthroid"  ,Id = Guid.Parse("{16D10CA5-1824-4056-88C9-D6829F8ED67A}") }; 
            yield return  new Bot() { Name = "PRO-EX"     ,Id = Guid.Parse("{735B9D1E-7938-4EEA-A40B-145C270A52C2}") };
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

       
    }
}