﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Service.Test.Mock.Data
{
    internal class MessageTestData : IEnumerable<object[]>
    {
       
        public IEnumerator<object[]> GetEnumerator()
        {            
            return GetMessages().Select(message => new object[] { message }).GetEnumerator();
        }


        public IEnumerable<Message> GetMessages()
        {
            yield return new Message() {
                Id = Guid.Parse("{0DF2FDDA-DB71-4A66-B36D-46A62F41604E}"),
                ConversationId = Guid.Parse("{D9F5304D-6C45-4389-B778-624D45425302}"),               
                Timestamp = DateTime.Parse("2018-11-16T23:30:52.6917722Z"),
                From = Guid.Parse("{C93AFB6F-A46F-4E4A-BBB5-C36AFAB6BF2B}"),
                To = Guid.Parse("07307036-ED50-4031-8335-2C1ECFCC8DD7"),
                Text = "Oi! Como posso te ajudar?",
            };

            yield return new Message()
            {
                Id = Guid.Parse("{9DA329FD-65A6-4A62-BC5E-BB18D2CCA46E}"),
                ConversationId = Guid.Parse("{D9F5304D-6C45-4389-B778-624D45425302}"),
                Timestamp = DateTime.Parse("2018-11-16T23:30:52.6917722Z"),
                From = Guid.Parse("{16D10CA5-1824-4056-88C9-D6829F8ED67A}"),
                To = Guid.Parse("{C93AFB6F-A46F-4E4A-BBB5-C36AFAB6BF2B}"),
                Text = "Gostaria de saber meu saldo ? ",
            };

            yield return new Message()
            {
                Id = Guid.Parse("{066B04C3-8A87-4CA6-9DA9-3F66D2D2B1EE}"),
                ConversationId = Guid.Parse("{D9F5304D-6C45-4389-B778-624D45425302}"),
                Timestamp = DateTime.Parse("2018-11-16T23:30:52.6917722Z"),
                From = Guid.Parse("{C93AFB6F-A46F-4E4A-BBB5-C36AFAB6BF2B}"),
                To = Guid.Parse("{384B56F1-D17F-4250-BB0B-14FF9C21C8DB}"),
                Text = "Seu saldo é de R$1,00",
            };
           
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

       
    }
}