﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Vivo.Challenge.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace Vivo.Challenge.Service.Test.Mock.Repository
{
    internal static class ConfigBotRepositoryMockExtension
    {

        internal static IBotRepository ConfigBotRepositoryMock(this IServiceProvider services)
        {
            var mock = services.GetService<Mock<IBotRepository>>();
            SetupCreate(mock);
            SetupGetAll(services, mock);
            SetupGet(services, mock);
            SetupUpdate(services, mock);
            SetupDelete(services, mock);

            return mock.Object;
        }

        private static void SetupDelete(IServiceProvider services, Mock<IBotRepository> mock)
        {
            mock.Setup(repository =>
                             repository.Delete(It.IsAny<Guid>())
                        ).Returns<Guid>((id) =>
                        {
                            var data = services.GetService<List<Bot>>();
                            data.RemoveAll(x => x.Id == id);
                            return Task.CompletedTask;
                        });
        }

        private static void SetupUpdate(IServiceProvider services, Mock<IBotRepository> mock)
        {
            mock.Setup(repository =>
                            repository.Update(It.IsAny<Bot>())
                       ).Returns<Bot>((bot) =>
                       {
                           var data = services.GetService<List<Bot>>();
                           var entity = data.FirstOrDefault(x => x.Id == bot.Id);
                           if (entity != null)
                               entity.Name = bot.Name;
                           return Task.CompletedTask;
                       });
        }

        private static void SetupGet(IServiceProvider services, Mock<IBotRepository> mock)
        {
            mock.Setup(repository =>
                            repository.Get(It.IsAny<Guid>())
                       ).Returns<Guid>((id) =>
                       {
                           var data = services.GetService<List<Bot>>();
                           return Task.FromResult(data.FirstOrDefault(x => x.Id == id));
                       });
        }

        private static void SetupGetAll(IServiceProvider services, Mock<IBotRepository> mock)
        {
            mock.Setup(repository =>
               repository.Get()
          ).Returns(() =>
          {
              var data = services.GetService<List<Bot>>();
              return Task.FromResult(data.Select(x => x));
          });
        }

        private static void SetupCreate(Mock<IBotRepository> mock)
        {
            mock.Setup(repository =>
                             repository.Create(It.IsAny<Bot>())
                        ).Returns<Bot>((bot) => Task.FromResult(bot));
        }
    }
}
