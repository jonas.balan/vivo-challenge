﻿using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Vivo.Challenge.Repository;
using System.Linq;
using Vivo.Challenge.Service.Test.Mock.Data;

namespace Vivo.Challenge.Service.Test.Mock.Repository
{
    public static class ConfigDIRepositoriesMockExtension
    {
        public static void ConfigDIRepositoriesMock(this IServiceCollection services)
        {
            services.AddScoped<BotTestData>();
            services.AddScoped<List<Bot>>(x => x.GetService<BotTestData>().GetBots().ToList());
            services.AddScoped<Mock<IBotRepository>>();
            services.AddScoped(x => x.ConfigBotRepositoryMock());

            services.AddScoped<MessageTestData>();
            services.AddScoped<List<Message>>(x => x.GetService<MessageTestData>().GetMessages().ToList());
            services.AddScoped<Mock<IMessageRepository>>();
            services.AddScoped(x => x.ConfigMessageRepositoryMock());
        }      


        

        
    }


}
