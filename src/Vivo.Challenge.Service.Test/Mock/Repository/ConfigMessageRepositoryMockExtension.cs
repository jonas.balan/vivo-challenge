﻿
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Vivo.Challenge.Repository;

namespace Vivo.Challenge.Service.Test.Mock.Repository
{
    internal static class ConfigMessageRepositoryMockExtension
    {
        internal static IMessageRepository ConfigMessageRepositoryMock(this IServiceProvider services)
        {
            var mock = services.GetService<Mock<IMessageRepository>>();
            SetupCreate(mock);
            SetupGetAll(services, mock);
            SetupGet(services, mock);
            SetupUpdate(services, mock);
            SetupDelete(services, mock);

            return mock.Object;
        }

        private static void SetupDelete(IServiceProvider services, Mock<IMessageRepository> mock)
        {
            mock.Setup(repository =>
                 repository.Delete(It.IsAny<Guid>())
            ).Returns<Guid>((id) =>
            {
                var data = services.GetService<List<Message>>();
                data.RemoveAll(x => x.Id == id);
                return Task.CompletedTask;
            });
        }

        private static void SetupUpdate(IServiceProvider services, Mock<IMessageRepository> mock)
        {
            mock.Setup(repository =>
                            repository.Update(It.IsAny<Message>())
                       ).Returns<Message>((message) =>
                       {
                           var data = services.GetService<List<Message>>();
                           var entity = data.FirstOrDefault(x => x.Id == message.Id);
                           if (entity != null)
                               entity.Text = message.Text;
                           return Task.CompletedTask;
                       });
        }

        private static void SetupGet(IServiceProvider services, Mock<IMessageRepository> mock)
        {
            mock.Setup(repository =>
                            repository.Get(It.IsAny<Guid>())
                       ).Returns<Guid>((id) =>
                       {
                           var data = services.GetService<List<Message>>();
                           return Task.FromResult(data.FirstOrDefault(x => x.Id == id));
                       });
        }

        private static void SetupGetAll(IServiceProvider services, Mock<IMessageRepository> mock)
        {
            mock.Setup(repository =>
               repository.Get()
          ).Returns(() =>
          {
              var data = services.GetService<List<Message>>();
              return Task.FromResult(data.Select(x => x));
          });
        }

        private static void SetupCreate(Mock<IMessageRepository> mock)
        {
            mock.Setup(repository =>
                             repository.Create(It.IsAny<Message>())
                        ).Returns<Message>((message) => Task.FromResult(message));
        }
    }
}
