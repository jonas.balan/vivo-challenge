﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Common;
using Vivo.Challenge.Model;
using Vivo.Challenge.Repository;

namespace Vivo.Challenge.Service
{
    public class BaseService<T> : IBaseService<T> where T : BaseEntity
    {
        protected readonly IBaseRepository<T> _baseRepository;
        public BaseService(IBaseRepository<T> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public virtual async Task<IEnumerable<T>> Get()
        {
            return await _baseRepository.Get();
        }

        public virtual  async Task<T> Get(Guid id)
        {
            return await _baseRepository.Get(id);
        }

        public virtual async Task<T> Create(T entity)
        {
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();
            return await _baseRepository.Create(entity);
        }

        public virtual async Task Update(T entity)
        {
            await _baseRepository.Update(entity);
        }

        public virtual async Task Delete(Guid id)
        {
            await _baseRepository.Delete(id);
        }
    }
}
