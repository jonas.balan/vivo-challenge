﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Vivo.Challenge.Repository;

namespace Vivo.Challenge.Service
{
    public class BotService : BaseService<Bot>, IBotService
    {
        public BotService(IBotRepository botRepository) : base(botRepository)
        {

        }
    }
}