﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Vivo.Challenge.Service
{
    public static class ConfigDIServicesExtension
    {
        public static void ConfigDIServices(this IServiceCollection services)
        {
            services.AddScoped<IBotService, BotService>();
            services.AddScoped<IMessageService, MessageService>();
        }
    }
}
