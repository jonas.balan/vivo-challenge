﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;

namespace Vivo.Challenge.Service
{
    public interface IMessageService : IBaseService<Message>
    {
        Task<IEnumerable<Message>> GetByConversationId(Guid conversationId);
    }
}
