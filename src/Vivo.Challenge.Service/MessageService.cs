﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivo.Challenge.Model;
using Vivo.Challenge.Repository;

namespace Vivo.Challenge.Service
{
    public class MessageService : BaseService<Message>, IMessageService
    {
        private readonly IBotService _botService;
        public MessageService(IMessageRepository repository, IBotService botService) : base(repository)
        {
            _botService = botService;
        }

        public override async Task<Message> Create(Message entity)
        {          
            entity.BotMessageDirection = await IdentifyMessageBotDirection(entity);
            return await base.Create(entity);
        }

        public async Task<IEnumerable<Message>> GetByConversationId(Guid conversationId)
        {
            return await ((IMessageRepository)_baseRepository).GetByConversationId(conversationId);
        }

        private async Task<BotMessageDirectionEnum> IdentifyMessageBotDirection(Message message)
        {
            var botFromDirection = await _botService.Get(message.From);
            if (botFromDirection != null)
                return BotMessageDirectionEnum.From;
            else
            {
                var botToDirection = await _botService.Get(message.To);
                if (botToDirection != null)
                    return BotMessageDirectionEnum.To;
                else
                    throw new Exception("No bot was identified in this message");
            }
        }        
    }
}
